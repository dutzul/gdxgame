package com.mygdx.game;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Graphics;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;

import org.w3c.dom.css.Counter;

import java.util.ArrayList;
import java.util.TreeMap;


public class MyGdxGame extends ApplicationAdapter {
	SpriteBatch batch;
	World world;
	int cnt=0;
	ArrayList<Sprite> mySprites;
	ArrayList<Body> myBodies;
	Texture backgroundTexture;
	int srcx=0;
	boolean fullScreen=false;
	Texture ButtonUP;
	Texture ButtonDOWN;
	Texture ButtonMid;
    private ShapeRenderer shapeRenderer;
    int is_shooting;
    ArrayList<Body> bullets;
    Texture BulletTexture;
    ArrayList<Body> randomJunk;
    ShapeRenderer renderer;
    ArrayList<Texture> textureArrayList;
	ArrayList<AnimationDraw> stateDraw;
    int junkcounter=0;
    class AnimationDraw{
        public Texture tex;
        public int counter;
        public int maxCounter;
        public int w;
        public int h;
        int nrLines;
        int nrCols;

        public boolean is_done()	{
            if (counter==maxCounter){
                return true;
            }
            return false;
        }
        public  AnimationDraw(Texture tex,int nrLines,int nrCols,int maxCounter){
            this.counter=0;
            this.tex=tex;
            this.counter=counter;
            this.w=tex.getWidth();
            this.h=tex.getHeight();
            this.maxCounter=maxCounter;
            this.nrLines=nrLines;
            this.nrCols=nrCols;
        }
        public void update(){
            ++counter;
        }


        int posXtoDraw,posYtoDraw;
        void setPos(int posXtoDraw,int posYtoDraw){
        	this.posXtoDraw=posXtoDraw;
        	this.posYtoDraw=posYtoDraw;
		}

        TextureRegion getNextTextureRegion(){

            int ind_row_now=counter/nrCols;
            int ind_column_now=counter%nrCols;
            int Lrow=tex.getHeight()/nrLines;
            int Lcolumn=tex.getWidth()/nrCols;

            TextureRegion texreg=new TextureRegion(tex,Lrow*ind_row_now,Lcolumn*ind_column_now,
                    Lrow*ind_row_now+Lrow,Lcolumn*ind_column_now+Lcolumn);

            this.update();
            return texreg;
        }
    }

	Sprite getSpriteFromImage(String path){
		Texture img = new Texture(path);
		Sprite sprite = new Sprite(img);
		sprite.setPosition(100,100);
		sprite.setOrigin(sprite.getWidth()/2,sprite.getHeight()/2);
		return sprite;
	}

	Body getBodyDefFromSprite(Sprite sprite,int posx,int posy){
		Body body;
		BodyDef bodyDef = new BodyDef();
		bodyDef.type = BodyDef.BodyType.DynamicBody;
		bodyDef.position.set(posx, posy);
		body = world.createBody(bodyDef);
		CircleShape shape = new CircleShape();
		shape.setRadius(100);
		FixtureDef fixtureDef = new FixtureDef();
		fixtureDef.shape = shape;
		fixtureDef.density = 0.5f;
		fixtureDef.friction=0.1f;
		Fixture fixture = body.createFixture(fixtureDef);
		shape.dispose();

		body.setUserData("mainPlane");
        System.out.println(body.getUserData());
		return body;
	}

	Texture readAndResizePixmap(String path){
        Pixmap pixmapinitial = new Pixmap(Gdx.files.internal(path));
        Pixmap pixmapfinal = new Pixmap(Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), pixmapinitial.getFormat());
        pixmapfinal.drawPixmap(pixmapinitial,
                0, 0, pixmapinitial.getWidth(), pixmapinitial.getHeight(),
                0, 0, pixmapfinal.getWidth(), pixmapfinal.getHeight()
        );
        Texture texture = new Texture(pixmapfinal);
        return  texture;
    }

    public Body CreateBullet(int posx,int posy,int dirx,int diry,int radius){
		Body body=null;
		BodyDef bodyDef = new BodyDef();
		bodyDef.type = BodyDef.BodyType.DynamicBody;
		bodyDef.position.set(posx, posy);
		body = world.createBody(bodyDef);

		CircleShape circle = new CircleShape();
		circle.setRadius(radius);

		FixtureDef fixtureDef = new FixtureDef();
		fixtureDef.shape =circle;
		fixtureDef.density = 0.5f;
		fixtureDef.friction=0.1f;
		//fixtureDef.filter.maskBits=0x0000;
		Fixture fixture = body.createFixture(fixtureDef);
		body.setLinearVelocity(dirx*100,0);

		circle.dispose();
		body.setGravityScale(0);
		return body;
	}

	Body createRandomJunk(int y,int radius){
		Body body=CreateBullet(Gdx.graphics.getWidth(),y,-10,0,radius);
		body.setUserData("Junk"+Integer.toString(junkcounter++));
		return body;
	}

    void addWorldListener(){
        world.setContactListener(new ContactListener() {
            @Override
            public void beginContact(Contact contact) {
				Body body1=contact.getFixtureA().getBody();
				Body body2=contact.getFixtureB().getBody();
				if(body1.getUserData().toString().startsWith("junk")) {
					if (randomJunk.remove(body1)){
						AnimationDraw myAnimationDraw=new AnimationDraw(new Texture("Explosion.png"),4,3,12);
						myAnimationDraw.setPos((int)body1.getPosition().x,(int)body1.getPosition().y);
						stateDraw.add(myAnimationDraw);
					}
				}

				if(body2.getUserData().toString().startsWith("junk")) {
					if (randomJunk.remove(body2)){
						AnimationDraw myAnimationDraw=new AnimationDraw(new Texture("Explosion.png"),4,3,12);
						myAnimationDraw.setPos((int)body2.getPosition().x,(int)body2.getPosition().y);
						stateDraw.add(myAnimationDraw);
					}
				}
            }

            @Override
            public void endContact(Contact contact) {

            }

            @Override
            public void postSolve(Contact arg0, ContactImpulse arg1) {
                // TODO Auto-generated method stub
            }

            @Override
            public void preSolve(Contact arg0, Manifold arg1) {
                // TODO Auto-generated method stub
            }
        });

    }
	@Override
	public void create () {

	   // stateDraw=new ArrayList<PairTextureInteger>();
		batch = new SpriteBatch();
		bullets=new ArrayList<Body>();
		randomJunk= new ArrayList<Body>();
		renderer = new ShapeRenderer();
		textureArrayList=new ArrayList<Texture>();

		String desktop_or_android="";
		for(int i=0;i<7;++i){
			textureArrayList.add(new Texture(desktop_or_android+"junk"+Integer.toString(i+1)+".png"));
		}

		Gdx.gl.glClearColor(0, 0, 1, 0);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		Gdx.graphics.setResizable(true);

		ButtonUP=new Texture("UP.png");
        ButtonDOWN=new Texture("DOWN.png");
        ButtonMid=new Texture("Fire.png");
        BulletTexture=new Texture("Bullet.png");


        shapeRenderer = new ShapeRenderer();

		backgroundTexture=readAndResizePixmap("Background.jpg");
        backgroundTexture.setWrap(Texture.TextureWrap.Repeat,Texture.TextureWrap.Repeat);

		mySprites=new ArrayList<Sprite>();
		myBodies= new ArrayList<Body>();
		mySprites.add(getSpriteFromImage("Fly1.png"));
		mySprites.add(getSpriteFromImage("Fly2.png"));


        mySprites.add(getSpriteFromImage("Shoot5.png"));
        mySprites.add(getSpriteFromImage("Shoot4.png"));
        mySprites.add(getSpriteFromImage("Shoot3.png"));
        mySprites.add(getSpriteFromImage("Shoot2.png"));
		mySprites.add(getSpriteFromImage("Shoot1.png"));


        world = new World(new Vector2(0, -0f), true);
		myBodies.add(getBodyDefFromSprite(mySprites.get(0),Gdx.graphics.getWidth()/5,Gdx.graphics.getHeight()/2));
        myBodies.get(0).setLinearVelocity(new Vector2(new Vector2(0, 1000)));
       // addWorldListener();
	}

	@Override
	public void render () {

		if (Math.random() <0.01 ){
			randomJunk.add(createRandomJunk((int)(Gdx.graphics.getHeight()*Math.random()),50));
		}

		if (myBodies.get(0).getPosition().y> Gdx.graphics.getHeight()-Gdx.graphics.getHeight()/7){
			myBodies.get(0).setLinearVelocity(0f,-10);
		}

		if (myBodies.get(0).getPosition().y<0){
			myBodies.get(0).setLinearVelocity(0f,10);
		}


        float to_jump=0;

        if (Gdx.input.isKeyPressed(Input.Keys.TAB)) {
            fullScreen = !fullScreen;
            Graphics.DisplayMode currentMode = Gdx.graphics.getDisplayMode();
            Gdx.graphics.setFullscreenMode(currentMode);
        }


        if (Gdx.input.isTouched()){
            if (Gdx.input.getX()<=Gdx.graphics.getWidth()/6){
                int y=Gdx.input.getY();

                if (y<Gdx.graphics.getHeight()/3&&myBodies.get(0).getPosition().y< Gdx.graphics.getHeight()-Gdx.graphics.getHeight()/7) {
                    to_jump = 0.5f;
                    myBodies.get(0).setAngularVelocity(0f);
                    myBodies.get(0).setLinearVelocity(0,100*to_jump);
                }else{

                    if (y>Gdx.graphics.getHeight()*2/3&&myBodies.get(0).getPosition().y>Gdx.graphics.getHeight()/10&&y>Gdx.graphics.getHeight()*2/3) {
						to_jump = -0.5f;
						myBodies.get(0).setAngularVelocity(0f);
						myBodies.get(0).setLinearVelocity(0, 100 * to_jump);
					}
                    else{
						if (y>Gdx.graphics.getHeight()/3&&y<Gdx.graphics.getHeight()*2/3) {
							if (is_shooting == 0) {
								is_shooting = 10;
								bullets.add(CreateBullet((int) myBodies.get(0).getPosition().x + Gdx.graphics.getWidth() / 7, (int) myBodies.get(0).getPosition().y + Gdx.graphics.getHeight() / 20, 1000, 0,6));
							}
						}
					}
                }

            }
        }

		++cnt;

		world.step(Gdx.graphics.getDeltaTime()*1f, 30, 30);
		world.step(Gdx.graphics.getDeltaTime()*1f, 30, 30);
		world.step(Gdx.graphics.getDeltaTime()*1f, 30, 30);
		world.step(Gdx.graphics.getDeltaTime()*1f, 30, 30);
		for(int i=0;i<mySprites.size();++i) {
            mySprites.get(i).setPosition(myBodies.get(0).getPosition().x, myBodies.get(0).getPosition().y);
        }


		Gdx.gl.glClearColor(0, 0, 1, 0);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		batch.begin();

		batch.draw(backgroundTexture,0, 0, srcx , 0, Gdx.graphics.getWidth(),
                Gdx.graphics.getHeight());
        srcx +=3;

        batch.draw(ButtonUP,0,(int)((float)Gdx.graphics.getHeight()*1.3/2.0)+80,Gdx.graphics.getWidth()/6,Gdx.graphics.getHeight()/4);
        batch.draw(ButtonDOWN,0,0,Gdx.graphics.getWidth()/6,Gdx.graphics.getHeight()/4);
		batch.draw(ButtonMid,0,Gdx.graphics.getHeight()/2.5f,Gdx.graphics.getWidth()/5,Gdx.graphics.getHeight()/4);


		if (is_shooting==0) {
			if (cnt % 3 == 0)
				batch.draw(mySprites.get(0), mySprites.get(0).getX(), mySprites.get(0).getY(), 0, 0, Gdx.graphics.getWidth() / 5, Gdx.graphics.getHeight() / 6, 1, 1, 0);
			else
				batch.draw(mySprites.get(1), mySprites.get(1).getX(), mySprites.get(1).getY(), 0, 0, Gdx.graphics.getWidth() / 5, Gdx.graphics.getHeight() / 6, 1, 1, 0);
		}else{
			is_shooting--;

            batch.draw(mySprites.get(is_shooting/2+1), mySprites.get(is_shooting/2+1).getX(), mySprites.get(is_shooting/2+1).getY(), 0, 0, Gdx.graphics.getWidth() / 5, Gdx.graphics.getHeight() / 6, 1, 1, 0);
		}
		for(int i=0;i<bullets.size();++i){
			batch.draw(BulletTexture,bullets.get(i).getPosition().x, bullets.get(i).getPosition().y,Gdx.graphics.getWidth()/15,Gdx.graphics.getHeight()/25);
		}


		for(int i=0;i<randomJunk.size();++i)
			if (randomJunk.get(i).getPosition().x>-150){
				batch.draw(textureArrayList.get(i%textureArrayList.size()),randomJunk.get(i).getPosition().x,
				randomJunk.get(i).getPosition().y,100,100);
			}


		batch.end();
	}
	
	@Override
	public void dispose () {
		batch.dispose();
		//img.dispose();
		world.dispose();
	}
}
